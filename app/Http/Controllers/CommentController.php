<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Comment::orderBy('created_at', 'DESC')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newComment = new Comment;
        $newComment->author = $request->comment['author'];
        $newComment->comment = $request->comment['comment'];
        $newComment->ticket_id = $request->comment['ticket_id'];
        $newComment->save();

        return $newComment;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $Comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comment = Comment::find($id);

        if($comment) {
            $comment->name = $request->comment['name'];
            $comment->description = $request->comment['description'];
            $comment->save();
            return $comment;
        }

        return "Comment not found.";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $Comment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::find($id);

        if($comment) {
            $comment->delete();
            return "Comment has been deleted.";
        }

        return "Comment not found.";
    }
}
