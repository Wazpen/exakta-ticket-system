<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Ticket::orderBy('created_at', 'DESC')->with('comments')->get();
    }

    public function filter(Request $request) 
    {
        $value = $request->query('value');
        return Ticket::where('status', '=', $value)->orderBy('created_at', 'DESC')->with('comments')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['data'] = json_decode($request['data']);

        if($request->file) {
            $paths = [];
            foreach ($request->file as $key => $file) {
                $filePath = $file->store('attachments', 'public');
                $paths[] = $filePath;
            }
            $attachments = json_encode($paths);    
        } else {
            $attachments = null;
        }
        
        $newTicket = new Ticket;
        $newTicket->name = $request->data->name;
        $newTicket->description = $request->data->description;
        $newTicket->attachments = $attachments;
        $newTicket->save();

        return $newTicket;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket = Ticket::with('comments')->find($id);

        return $ticket;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = Ticket::find($id);        
        $request['data'] = json_decode($request['data']);

        if($ticket) {
            if($request->file) {
                $paths = [];
                foreach ($request->file as $key => $file) {
                    $filePath = $file->store('attachments', 'public');
                    $paths[] = $filePath;
                }

                if($request->data->attachments) {
                    $existing_attachments = json_decode($request->data->attachments);
                    $image_paths = array_merge($existing_attachments, $paths);
                    $attachments = json_encode($image_paths);
                } else {
                    $attachments = $paths;
                }
            } else {
                $attachments = $request->data->attachments;
            }
            
            $ticket->name = $request->data->name;
            $ticket->description = $request->data->description;
            $ticket->attachments = $attachments;
            $ticket->save();
            return $ticket;
        }

        return "Ticket not found.";
    }

    public function toggleStatus(Request $request, $id)
    {
        $ticket = Ticket::find($id);

        if($ticket) {
            if($ticket->status === "pending" ) {
                $newStatus = "done";
            }

            if($ticket->status === "done") {
                $newStatus = "pending";
            }

            $ticket->status = $newStatus;
            $ticket->save();
            return $ticket;
        }

        return "Ticket not found.";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket = Ticket::find($id);

        if($ticket) {
            $ticket->delete();
            return "Ticket has been deleted.";
        }

        return "Ticket not found.";
    }
}
