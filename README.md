
## Steg 1

<p>Kopiera .env.example och döp den till .env och lägg den i rot-mappen.</p>

## Steg 2
<p>Skapa en databas för projektet.</p>

## Steg 3
<p>Fyll i databasen i .env filen.<br>
Databasnamnet i DB_DATABASE <br>
User i DB_USERNAME <br>
Lösenord för användaren i DB_PASSWORD <br>
</p>

## Steg 4
<p>Kör "composer install" i rotmappen </p>

## Steg 5
<p>Kör "php artisan migrate"</p>


## Steg 6 
<p>Kör "php artisan key:generate" </p>

<p>Nu bör applikationen fungera.</p> 
<p>Ifall det skulle va så att bilderna inte fungerar när man laddat upp dem kan man behöva köra: "php artisan storage:link"</p>
