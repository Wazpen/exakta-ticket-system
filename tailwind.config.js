const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.gray,
      emerald: colors.emerald,
      yellow: colors.yellow,
      red: colors.red,
      amber: colors.amber,
      teal: colors.teal,
    },
    container: {
      center: true,
    },
    extend: {},
  },
  plugins: [],
}
