<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/tickets', [TicketController::class, 'index']);
Route::prefix('/ticket')->group( function () {
        Route::post('/store', [TicketController::class, 'store']);
        Route::post('/{id}', [TicketController::class, 'update']);
        Route::get('/{id}', [TicketController::class, 'show']);
        Route::put('/status/{id}', [TicketController::class, 'toggleStatus']);
        Route::delete('/{id}', [TicketController::class, 'destroy']);
    }
);
Route::get('/filter', [TicketController::class, 'filter']);

Route::post('/comment/store', [CommentController::class, 'store']);

